# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-camera package.
#
# Stefan Asserhäll <stefan.asserhall@gmail.com>, 2019, 2020, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-camera\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-02-06 00:42+0000\n"
"PO-Revision-Date: 2023-10-27 07:06+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@gmail.com>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Stefan Asserhäll"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "stefan.asserhall@gmail.com"

#: src/contents/ui/CameraPage.qml:26 src/contents/ui/GlobalDrawer.qml:20
#: src/contents/ui/main.qml:14
#, kde-format
msgid "Camera"
msgstr "Kamera"

#: src/contents/ui/CameraPage.qml:43
#, kde-format
msgid "Switch mode"
msgstr "Byt läge"

#: src/contents/ui/CameraPage.qml:66
#, kde-format
msgid "Cancel self-timer"
msgstr "Avbryt självutlösare"

#: src/contents/ui/CameraPage.qml:68
#, kde-format
msgid "Capture photo"
msgstr "Ta foto"

#: src/contents/ui/CameraPage.qml:70
#, kde-format
msgid "Stop recording video"
msgstr "Stoppa inspelning av video"

#: src/contents/ui/CameraPage.qml:72
#, kde-format
msgid "Start recording video"
msgstr "Starta inspelning av video"

#: src/contents/ui/CameraPage.qml:107
#, kde-format
msgid "Switch Camera"
msgstr "Byt kamera"

#: src/contents/ui/CameraPage.qml:176
#, kde-format
msgid "Camera not available"
msgstr "Kamera inte tillgänglig"

#: src/contents/ui/CameraPage.qml:178
#, kde-format
msgid "Camera is busy. Is another application using it?"
msgstr "Kameran är upptagen. Används den av ett annat program?"

#: src/contents/ui/CameraPage.qml:180
#, kde-format
msgid "Missing camera resource."
msgstr "Saknar kameraresurs."

#: src/contents/ui/CameraPage.qml:446
#, kde-format
msgid "Photo"
msgstr "Foto"

#: src/contents/ui/CameraPage.qml:451
#, kde-format
msgid "Video"
msgstr "Video"

#: src/contents/ui/CameraPage.qml:554
#, kde-format
msgid "Took a photo"
msgstr "Tog ett foto"

#: src/contents/ui/CameraPage.qml:556
#, kde-format
msgid "Failed to take a photo"
msgstr "Misslyckades ta ett foto"

#: src/contents/ui/CameraPage.qml:561
#, kde-format
msgid "Stopped recording"
msgstr "Stoppade inspelning"

#: src/contents/ui/CameraPage.qml:565
#, kde-format
msgid "Started recording"
msgstr "Startade inspelning"

#: src/contents/ui/CameraPage.qml:567
#, kde-format
msgid "Failed to start recording"
msgstr "Misslyckades starta inspelning"

#: src/contents/ui/GlobalDrawer.qml:37
#, kde-format
msgid "Resolution"
msgstr "Upplösning"

#: src/contents/ui/GlobalDrawer.qml:56
#, kde-format
msgid "White balance"
msgstr "Vitbalans"

#: src/contents/ui/GlobalDrawer.qml:62
#, kde-format
msgid "Auto"
msgstr "Automatisk"

#: src/contents/ui/GlobalDrawer.qml:69
#, kde-format
msgid "Sunlight"
msgstr "Solljus"

#: src/contents/ui/GlobalDrawer.qml:76
#, kde-format
msgid "Cloudy"
msgstr "Molnigt"

#: src/contents/ui/GlobalDrawer.qml:83
#, kde-format
msgid "Tungsten"
msgstr "Wolfram"

#: src/contents/ui/GlobalDrawer.qml:90
#, kde-format
msgid "Fluorescent"
msgstr "Fluorescerande"

#: src/contents/ui/GlobalDrawer.qml:96
#, kde-format
msgid "About"
msgstr "Om"

#: src/contents/ui/main.qml:52
#, kde-format
msgid ""
"An error occurred: \"%1\". Please consider restarting the application if it "
"stopped working."
msgstr ""
"Ett fel uppstod: \"%1\". Överväg att starta om programmet om det slutat "
"fungera."

#: src/main.cpp:45
#, kde-format
msgid "© Plasma Mobile Developers"
msgstr "© Plasma mobilutvecklare"

#: src/main.cpp:47
#, kde-format
msgid "Marco Martin"
msgstr "Marco Martin"

#: src/main.cpp:48
#, kde-format
msgid "Jonah Brüchert"
msgstr "Jonah Brüchert"

#~ msgid "Self-timer"
#~ msgstr "Självutlösare"

#~ msgid "Off"
#~ msgstr "Av"

#~ msgid "2 s"
#~ msgstr "2 s"

#~ msgid "5 s"
#~ msgstr "5 s"

#~ msgid "10 s"
#~ msgstr "10 s"

#~ msgid "20 s"
#~ msgstr "20 s"

#~ msgid "An error occurred: \""
#~ msgstr "Ett fel uppstod:  \""

#~ msgid "Simple camera application"
#~ msgstr "Enkelt kameraprogram"

#~ msgid "License:"
#~ msgstr "Licens:"

#~ msgid "View source code online"
#~ msgstr "Visa källkoden på nätet"
