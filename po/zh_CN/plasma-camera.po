msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-02-06 00:42+0000\n"
"PO-Revision-Date: 2024-04-23 19:31\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/plasma-camera/plasma-camera.pot\n"
"X-Crowdin-File-ID: 53720\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Tyson Tan"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "tds00@qq.com"

#: src/contents/ui/CameraPage.qml:26 src/contents/ui/GlobalDrawer.qml:20
#: src/contents/ui/main.qml:14
#, kde-format
msgid "Camera"
msgstr "相机"

#: src/contents/ui/CameraPage.qml:43
#, kde-format
msgid "Switch mode"
msgstr "切换模式"

#: src/contents/ui/CameraPage.qml:66
#, kde-format
msgid "Cancel self-timer"
msgstr "取消自拍定时器"

#: src/contents/ui/CameraPage.qml:68
#, kde-format
msgid "Capture photo"
msgstr "拍摄照片"

#: src/contents/ui/CameraPage.qml:70
#, kde-format
msgid "Stop recording video"
msgstr "停止录制视频"

#: src/contents/ui/CameraPage.qml:72
#, kde-format
msgid "Start recording video"
msgstr "开始录制视频"

#: src/contents/ui/CameraPage.qml:107
#, kde-format
msgid "Switch Camera"
msgstr "切换相机"

#: src/contents/ui/CameraPage.qml:176
#, kde-format
msgid "Camera not available"
msgstr "相机不可用"

#: src/contents/ui/CameraPage.qml:178
#, kde-format
msgid "Camera is busy. Is another application using it?"
msgstr "相机正忙。请检查是否有其他应用正在使用相机。"

#: src/contents/ui/CameraPage.qml:180
#, kde-format
msgid "Missing camera resource."
msgstr "相机资源缺失。"

#: src/contents/ui/CameraPage.qml:446
#, kde-format
msgid "Photo"
msgstr "照片"

#: src/contents/ui/CameraPage.qml:451
#, kde-format
msgid "Video"
msgstr "视频"

#: src/contents/ui/CameraPage.qml:554
#, kde-format
msgid "Took a photo"
msgstr "拍摄照片"

#: src/contents/ui/CameraPage.qml:556
#, kde-format
msgid "Failed to take a photo"
msgstr "照片拍摄失败"

#: src/contents/ui/CameraPage.qml:561
#, kde-format
msgid "Stopped recording"
msgstr "已停止录像"

#: src/contents/ui/CameraPage.qml:565
#, kde-format
msgid "Started recording"
msgstr "已开始录像"

#: src/contents/ui/CameraPage.qml:567
#, kde-format
msgid "Failed to start recording"
msgstr "无法开始录像"

#: src/contents/ui/GlobalDrawer.qml:37
#, kde-format
msgid "Resolution"
msgstr "分辨率"

#: src/contents/ui/GlobalDrawer.qml:56
#, kde-format
msgid "White balance"
msgstr "白平衡"

#: src/contents/ui/GlobalDrawer.qml:62
#, kde-format
msgid "Auto"
msgstr "自动"

#: src/contents/ui/GlobalDrawer.qml:69
#, kde-format
msgid "Sunlight"
msgstr "日光"

#: src/contents/ui/GlobalDrawer.qml:76
#, kde-format
msgid "Cloudy"
msgstr "阴天"

#: src/contents/ui/GlobalDrawer.qml:83
#, kde-format
msgid "Tungsten"
msgstr "白炽灯"

#: src/contents/ui/GlobalDrawer.qml:90
#, kde-format
msgid "Fluorescent"
msgstr "荧光灯"

#: src/contents/ui/GlobalDrawer.qml:96
#, kde-format
msgid "About"
msgstr "关于"

#: src/contents/ui/main.qml:52
#, kde-format
msgid ""
"An error occurred: \"%1\". Please consider restarting the application if it "
"stopped working."
msgstr "发生错误：“%1”。如果应用已停止工作，请考虑重新启动它。"

#: src/main.cpp:45
#, kde-format
msgid "© Plasma Mobile Developers"
msgstr "© Plasma Mobile Developers"

#: src/main.cpp:47
#, kde-format
msgid "Marco Martin"
msgstr "Marco Martin"

#: src/main.cpp:48
#, kde-format
msgid "Jonah Brüchert"
msgstr "Jonah Brüchert"
